let input = document.getElementById('price');
let container = document.getElementById('container');


input.onblur = function () {
    let span = document.querySelectorAll("span");
    if (span.length) {
        span[0].remove();
    }
    if (isNaN(this.value) || this.value == "" || this.value < 0) {
        this.style.border = "1px solid red";
        let errorSpan = document.createElement('span');
        errorSpan.classList.add("error");
        container.appendChild(errorSpan);
        errorSpan.textContent = "Enter correct value of price";

    } else {
        this.style.border = "";
        this.style.color = "green";
        let priceSpan = document.createElement('span');
        priceSpan.classList.add("price");
        container.appendChild(priceSpan);
        priceSpan.innerHTML = `The price is: ${this.value}<a href="#" class="cross"><sup>&#10006</sup></a>`;

        let cross = document.getElementsByClassName("cross");
        cross[0].addEventListener("click", function () {
            this.parentElement.remove();
            input.value = ""

        });

    }
};

input.onfocus = function () {
    if (this.value || this.value == "") {
        this.style.border = "1px solid green";
        this.value = "";
    }
    let errorDel = document.getElementsByClassName("error");
    if (errorDel) {
        errorDel[0].remove();
    }
};





